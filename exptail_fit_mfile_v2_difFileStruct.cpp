//fixed issue with not checking for ends of arrays with avg. max algorithm
//changed from constant std. dev. to finding it for each WF
//added header to each file for version control


#include <iostream>
#include <fstream>
#include <TGraphErrors.h>
#include <TF1.h>


void exptail_fit_mfile_v2(string fname)//, Int_t init, Int_t num)
{
  Byte_t result;
  Int_t bd;
  Int_t ch;
  ULong64_t timR;
  ULong64_t timC;
  Int_t length;
  Int_t length_temp;

  Short_t* data;
  Short_t maxx;
  Short_t maxy;
  Int_t maxySum;
  Double_t* x;
  Double_t* y;
  Double_t* xe;
  Double_t* ye;
  TGraphErrors* fitgr;
  TF1* fit;
  Long64_t nentries;
  Double_t bmean;
  Double_t bexpect2;
  Double_t stdev;
  string stemp;

  Double_t fallTime;
  Double_t amplitude;
  Double_t chiSqu;
  Short_t doFreed;

  //for(Int_t i=0; i<num; ++i)
  {
    data=NULL;

    //stemp = fname + "_" + to_string(init+i) + ".bin";
    ifstream fin(fname,ios::binary);

    stemp = "../../../res_results/data/sim/1Kadc_40nsR_varF_2016N/fall/" + fname.substr(fname.find_last_of('/')+1,-1);// + "_"
          //+ to_string(init+i) + "_fall.bin";
    ofstream foutf(stemp,ios::binary);
    stemp = "../../../res_results/data/sim/1Kadc_40nsR_varF_2016N/avgh/" + fname.substr(fname.find_last_of('/')+1,-1);// + "_"
          //+ to_string(init+i) + "_avgh.bin";
    ofstream fouth(stemp,ios::binary);

    if(fin.is_open() && foutf.is_open() && fouth.is_open())
    {
      fin.seekg(37,fin.beg);
      fin.read((Char_t*)&length,4);
      length_temp = length;

      fin.seekg(0,fin.end);

      nentries=fin.tellg();
      nentries=(nentries-8)/(length*2+33);

      fin.seekg(8,fin.beg);

      length=0;
      foutf.write((Char_t*)&length,4);
      fouth.write((Char_t*)&length,4);

      for(Long64_t j=0; j<nentries; ++j)
      {
        fin.read((Char_t*)&result,1);
        fin.seekg(4,fin.cur);
        fin.read((Char_t*)&bd,4);
        fin.read((Char_t*)&ch,4);
        fin.read((Char_t*)&timR,8);
        fin.read((Char_t*)&timC,8);
        fin.read((Char_t*)&length,4);

        if(length_temp!=length || result==0)
        {
          stemp = "error_files/" + fname.substr(fname.find_last_of('/')+1,-1);// + "_"
                //+ to_string(init+i) + "_error.txt";
          ofstream foute(stemp,ios::app);
          foute << "Result 0 or Dif. Length Found at Entry " << j << "\n";
          j=nentries;
        }
        else
        {
          if(j==0)
            data = new Short_t[length];

          fin.read((Char_t*)data,length*2);

          for(UShort_t k=0; k<length; ++k)
          {
            data[k] = data[k] & 16383;

            if(data[k]>8191)
              data[k]-=16384;
          }

          bmean=0.;

          for(Short_t k=0; k<850; ++k)
          {
            bmean+=data[k];
            bexpect2+=data[k]*data[k];
          }

          bmean/=850.;
          bexpect2/=850.;
          stdev=sqrt(bexpect2-bmean*bmean);

          for(Short_t k=0; k<length; ++k)
            data[k]-=bmean;

          maxx=0;
          maxy=data[0];

          for(Short_t k=1; k<length; ++k)
            if(data[k]>maxy)
            {
              maxx=k;
              maxy=data[k];
            }

          if(maxx>4 && maxx<length-5)
          {
            maxySum=maxy;

            for(Short_t k=0; k<5; ++k)
            {
              maxySum+=data[maxx-k-1];
              maxySum+=data[maxx+k+1];
            }

            maxy=maxySum/11;
          }

          maxx+=200;

          if(maxx<length)
          {
            x = new Double_t[length-maxx];
            y = new Double_t[length-maxx];
            xe = new Double_t[length-maxx];
            ye = new Double_t[length-maxx];

            for(Short_t k=0; k<length-maxx; ++k)
            {
              x[k]=k;
              y[k]=data[maxx+k];
              xe[k]=0.;
              ye[k]=stdev;
            }

            fitgr = new TGraphErrors(length-maxx, x, y, xe, ye);
            fit = new TF1("fit", "[0]*exp(-x/[1])");
	          fit->SetParameters(y[0],1000.);
            fitgr->Fit("fit", "Q");

            fallTime=fit->GetParameter(1);
            amplitude=fit->GetParameter(0);
            chiSqu=fit->GetChisquare();
            doFreed=fit->GetNDF();
          }
          else
          {
            x=NULL;
            y=NULL;
            xe=NULL;
            ye=NULL;
            fitgr=NULL;
            fit=NULL;

            fallTime=-1.;
            amplitude=-1.;
            chiSqu=-1.;
            doFreed=-1;
          }

          maxx-=200;

          foutf.write((Char_t*)&fallTime,8);
          foutf.write((Char_t*)&amplitude,8);
          foutf.write((Char_t*)&chiSqu,8);
          foutf.write((Char_t*)&doFreed,2);
          foutf.write((Char_t*)&timR,8);

          fouth.write((Char_t*)&maxx,2);
          fouth.write((Char_t*)&maxy,2);
          fouth.write((Char_t*)&timR,8);

          if(x!=NULL)
          {
            delete[] x;
            delete[] y;
            delete[] xe;
            delete[] ye;
            delete fitgr;
            delete fit;
          }
        }
      }
    }
    else
    {
      stemp = "error_files/" + fname.substr(fname.find_last_of('/')+1,-1);// + "_"
            //+ to_string(init+i) + "_error.txt";
      ofstream foute(stemp,ios::app);
      foute << "Cannot Open File\n";
    }

    if(data!=NULL)
      delete[] data;
  }
}
